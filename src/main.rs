
/*  This is a cli project for boomers
**  who can't remember how old they are,
**  and who can remember their own birthdays.
*/

extern crate chrono;
use chrono::prelude::*;
use std::io;

fn main() {
    let current_year: i32 = Local::today().year();

    println!("In what year were you born?");
    let mut birth_year = String::new();
    io::stdin().read_line(&mut birth_year).unwrap();
    let birth_year: i32 = birth_year.trim().parse().unwrap();

    let age = current_year - birth_year;
    println!("You turned (or will turn) {} this year.", age);
}
